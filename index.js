// import chalk from 'chalk';
const fs = require('fs')
const readline = require('readline')
// const chalk = require('chalk')

const rl = readline.createInterface({
    input:process.stdin,
    output:process.stdout
});
const dirPath = './data'
if(!fs.existsSync(dirPath)){
    fs.mkdirSync(dirPath);
}
const dataPath = './data/contact.json';
if(!fs.existsSync(dataPath)){
    fs.writeFileSync(dataPath, '[]', 'utf-8')
}
rl.question('Masukan nama anda : ', (nama) => {
    rl.question('Masukan nomer anda : ', (noHP) => {
        const contact = {nama, noHP};
        const fileBuffer = fs.readFileSync('data/contact.json', 'utf-8');
        const contacts = JSON.parse(fileBuffer);

        contacts.push(contact);

        fs.writeFileSync('data/contact.json', JSON.stringify(contacts))
        console.log('ok')
        rl.close()
    })
})